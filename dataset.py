import itertools
import pathlib
import open3d as o3d
import torch
import numpy as np
import torch.utils.data
from scipy.spatial.transform import Rotation
from tqdm import tqdm


def read_pcd(filename, n_points):
    mesh = o3d.io.read_triangle_mesh(filename)
    pcd = mesh.sample_points_uniformly(number_of_points=n_points)
    return np.array(pcd.points).astype(np.float32)


def resample_pcd(pcd, n):
    """Drop or duplicate points so that pcd has exactly n points"""
    idx = np.random.permutation(pcd.shape[0])
    if idx.shape[0] < n:
        idx = np.concatenate([idx, np.random.randint(pcd.shape[0], size = n - pcd.shape[0])])
    return pcd[idx[:n]]


class PelvicBone(torch.utils.data.Dataset): 
    def __init__(
        self,
        pcd_filename: str,
        mirror_left=True,  # flip x axis of left bones to match right bones
        n_augmented=0,  # include additional random rotations and offsets
        offset_max=0.1,
        rot_max_angle=5,
        complete_points=8192,
        # partial_points=6800,
        missing_points=2048,
    ):
        complete = read_pcd(pcd_filename, complete_points)

        # normalize
        complete -= complete.mean(axis=0)
        complete /= np.abs(complete).max()

        if mirror_left and "left_pelvic" in pcd_filename:
            complete[:, 0] *= -1

        fragmentation = complete[:, 0] - complete[:, 1] < 0.3
        self.partial = resample_pcd(complete[fragmentation], complete_points - missing_points)
        self.missing = resample_pcd(complete[~fragmentation], missing_points)


        rotations = [Rotation.identity()] + \
            [Rotation.from_euler("xyz", np.random.uniform(-rot_max_angle, rot_max_angle, 3), degrees=True) for _ in range(n_augmented)]
        offsets = [np.zeros(3)] + [np.random.uniform(-offset_max, offset_max, 3) for _ in range(n_augmented)]
        self.items = list(zip(offsets, rotations))

    def __getitem__(self, index):
        offset, rotation = self.items[index]
        return (
            torch.FloatTensor(rotation.apply(self.partial) + offset),
            torch.FloatTensor(rotation.apply(self.missing) + offset),
        )

    def __len__(self):
        return len(self.items)


class PelvicBonesDataset(torch.utils.data.Dataset):
    def __init__(self, directory: str, **kwargs) -> None:
        self.dataset = torch.utils.data.ConcatDataset([
            PelvicBone(str(file), **kwargs)
            for file in tqdm(sorted(list(pathlib.Path(directory).glob("*.stl"))), desc=f"Loading {directory}")
        ])

    def __len__(self):
        return len(self.dataset)
    
    def __getitem__(self, index):
        return self.dataset[index]
