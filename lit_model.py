from typing import Any
import numpy as np
import torch
import torch.optim as optim
import sys
from utils import weights_init
sys.path.append("./emd/")
import emd_module as emd
import pytorch_lightning as pl


class LitMSN(pl.LightningModule):
    def __init__(self, num_points=8192, lr=0.001):
        super().__init__()
        self.save_hyperparameters()
        self.lr = lr

        from models.msn import MSN
        self.model = MSN(num_points = num_points)
        self.model.apply(weights_init)

        self.EMD = emd.emdModule()

    def forward(self, x):
        return self.model(x.transpose(1, 2))
    
    def compute_losses(self, x, y, eps=0.005, iters=50):
        output1, output2, expansion_penalty = self(x)

        y = y[:, :, :3]
        
        dist, _ = self.EMD(output1, y, eps, iters)
        emd1 = torch.sqrt(dist).mean(1)
        
        dist, _ = self.EMD(output2, y, eps, iters)
        emd2 = torch.sqrt(dist).mean(1)    

        loss = emd1.mean() + emd2.mean() + expansion_penalty.mean() * 0.1

        return loss, emd1, emd2, expansion_penalty

    def training_step(self, batch, batch_idx):
        x, y = batch
        loss, emd1, emd2, expansion_penalty = self.compute_losses(x, y)
        self.log("train_loss", loss)
        return loss
    
    def validation_step(self, batch, batch_idx):
        x, y = batch
        loss, emd1, emd2, expansion_penalty = self.compute_losses(x, y, eps=0.002, iters=10000)

        self.log("val_emd1", emd1.mean())
        self.log("val_emd2", emd2.mean())
        self.log("val_expansion_penalty", expansion_penalty.mean())
        self.log("val_loss", loss, prog_bar=True)
        
    def predict_step(self, batch, batch_idx):
        x, y = batch
        return self(x)
    
    def configure_optimizers(self):
        return optim.Adam(self.parameters(), lr=self.lr)



class LitDisp3D(pl.LightningModule):
    def __init__(self, encoder_params: dict, decoder_params: dict, lr=0.001, emd_eps=0.005, emd_iters=50):
        super().__init__()
        self.save_hyperparameters()
        self.lr = lr
        self.emd_eps = emd_eps
        self.emd_iters = emd_iters

        from models.displace.encode import Encoder
        from models.displace.decode import Decoder

        self.encoder = Encoder(**encoder_params)
        self.decoder = Decoder(**decoder_params)

        self.EMD = emd.emdModule()

    def forward(self, x):
        disp_feat, pcd_anchors = self.encoder(x)
        pred = self.decoder([disp_feat])
        return pred
    
    def compute_loss(self, x, y, eps=0.005, iters=50):
        pred = self(x)
        dist, _ = self.EMD(pred, y, eps, iters)
        emd = torch.sqrt(dist).mean()
        return emd

    def training_step(self, batch, batch_idx):
        x, y = batch
        loss = self.compute_loss(x, y, self.emd_eps, self.emd_iters)
        self.log("train_loss", loss)
        return loss
    
    def validation_step(self, batch, batch_idx):
        x, y = batch
        loss = self.compute_loss(x, y, eps=0.002, iters=10000)
        self.log("val_emd1", loss) # for comparison with MSN
        self.log("val_emd2", loss) # for comparison with MSN
        self.log("val_loss", loss, prog_bar=True)
        
    def predict_step(self, batch, batch_idx):
        x, y = batch
        return self(x)
    
    def configure_optimizers(self):
        return optim.Adam(self.parameters(), lr=self.lr)
