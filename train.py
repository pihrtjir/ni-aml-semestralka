import torch
import pytorch_lightning as pl
import wandb
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-g", "--gpus", type=int, nargs='+', required=True)
parser.add_argument("-n", "--name", type=str, required=True)
parser.add_argument("--wandb_key", type=str, default=None)
args = parser.parse_args()


# init dataset
from dataset import PelvicBonesDataset
train_loader = torch.utils.data.DataLoader(
    PelvicBonesDataset("data/train", n_augmented=20),
    batch_size=8,
    shuffle=True,
)
val_loader = torch.utils.data.DataLoader(
    PelvicBonesDataset("data/val"),
    batch_size=8,
)


# init model
from lit_model import LitMSN, LitDisp3D
model = LitDisp3D(
    encoder_params=dict(
        support_num=10,
        neighbor_num=20,
    ),
    decoder_params=dict(
        features=[1024, 256, 256, 256, 128, 128, 128, 3],
        degrees=[1, 2, 2, 2, 4, 4, 16],  # resulting number of points is equal to the product of all degrees
        support=10,
        root_num=1,
    ),
    lr=1e-5,
)
# model = LitMSN(lr=1e-4, num_points=2048)


# wandb
if args.wandb_key:
    wandb.login(relogin=True, force=True, key=args.wandb_key)
    wandb_logger = pl.loggers.WandbLogger(args.name, project="pelvic-bones")


# train
early_stopping_callback = pl.callbacks.EarlyStopping(monitor="val_emd2", mode="min", patience=5, strict=False)
model_checkpoint_callback = pl.callbacks.ModelCheckpoint(monitor="val_emd2", mode="min", save_top_k=1, save_weights_only=True)
trainer = pl.Trainer(
    callbacks=[early_stopping_callback, model_checkpoint_callback],
    logger=wandb_logger if args.wandb_key else None,
    accelerator="gpu",
    devices=args.gpus,
    max_epochs=-1,
    check_val_every_n_epoch=None,
    val_check_interval=100,
)
torch.set_float32_matmul_precision('medium')  # DGX tensor cores
trainer.fit(model, train_loader, val_loader)


# restore best weights
model = model.load_from_checkpoint(model_checkpoint_callback.best_model_path)


# log final metrics
if args.wandb_key:
    results = trainer.validate(model, val_loader)[0]
    wandb.log(results)
    wandb.finish(quiet=True)


# save weights
dest = f"weights/{args.name}.ckpt"
print(f"Saving checkpoint to {dest}")
trainer.save_checkpoint(dest, weights_only=True)
